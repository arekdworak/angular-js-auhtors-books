app.controller("AuthorsBooksCtrl", function ($scope, authorsBooksService, $filter, $q) {

    var self = this;

    /** @type {Array.<vo.Book>} */
    var books = [];

    /** @type {Array.<vo.Author>} */
    var authors = [];

    /** @type {Array.<string>} */
    self.nationalities = [];

    /** @type {string} */
    self.selectedNationality = "all";

    /** @type {string} */
    self.authorsQuery = "";

    /** @type {string} */
    self.booksQuery = "";

    /** @type {vo.Author} */
    self.selectedAuthor = null;


    /**
     * Returns filtered authors list
     * 
     * @returns {Array.<vo.Author>}
     */
    self.getFilteredAuthors = function () {
        var filteredAuthors = [];
        if (self.selectedNationality == "all") {
            filteredAuthors = authors;
        } else {
            filteredAuthors = authors.filter(function (author) {
                return author.nationality.toLowerCase() == self.selectedNationality;
            });
        }

        return $filter("filter")(filteredAuthors, function (author) {
            var normalizedQuery = self.authorsQuery.toLowerCase();
            return author.name.toLowerCase().indexOf(normalizedQuery) > -1 || author.nationality.toLowerCase().indexOf(normalizedQuery) > -1;
        });
    }


    /**
     * Returns filtered books list
     * 
     * @returns {Array.<vo.Book>}
     */
    self.getFilteredBooks = function () {
        var filteredAuthorsIds = self.selectedAuthor ? [self.selectedAuthor.id] : self.getFilteredAuthors().map(function (author) {
            return author.id;
        });
        var filteredBooks = $filter("filter")(books, function (book) {
            return filteredAuthorsIds.indexOf(book.authorId) > -1;
        });
        filteredBooks = $filter("filter")(filteredBooks, function (book) {
            var normalizedQuery = self.booksQuery.toLowerCase();
            return book.title.toLowerCase().indexOf(normalizedQuery) > -1 || book.year.indexOf(normalizedQuery) > -1;
        });
        return filteredBooks;
    }


    var init = function () {
        // init 3 arrays: authors, books, nationalities
        $q.all([authorsBooksService.getBooks(), authorsBooksService.getAuthors()]).then(function (results) {
            booksFromService = results[0];
            authorsFromService = results[1];
            authors = authorsFromService.map(function (author) {
                var booksCount = booksFromService.filter(function (book) {
                    return book.author_id == author.id
                }).length
                return new vo.Author(author.id, [author.first_name, author.last_name].join(" "), author.nationality, booksCount)
            });
            books = booksFromService.map(function (book) {
                var getAuthorName = function (book) {
                    var author = authors.filter(function (author) {
                        return author.id == book.author_id;
                    })[0];
                    return author ? author.name : "";
                };
                return new vo.Book(book.id, book.title, book.author_id, getAuthorName(book), book.year);
            })

            self.nationalities = authorsBooksService.getNationalitiesOfAuthors(authors);
        });

        // add watch to clear "click on author's book count effect"
        // when any of authors filter changed
        var watchAuthorFilters = $scope.$watchCollection(function () {
            return [self.selectedNationality, self.authorsQuery];
        }, function () {
            self.selectedAuthor = null;
        });

        // unregister watch when ctrl destroy
        $scope.$on("$destroy", watchAuthorFilters);
    }

    init();
});