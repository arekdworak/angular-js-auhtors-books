app.service("authorsBooksService", function ($http) {

    var self = this;

    /**
     * Returns books list from file
     * 
     * @returns {Array.<*>}
     */
    self.getBooks = function () {
        return loadFromJsonFile("../../data/books.json");
    }

    /**
     * Returns authors list from file
     * 
     * @returns {Array.<*>}
     */
    self.getAuthors = function () {
        return loadFromJsonFile("../../data/writers.json");
    }

    /**
     * Extract unique nationality values from authors list
     * 
     * @param authors {Array.<vo.Author>}
     * @returns {Array.<string>}
     */
    self.getNationalitiesOfAuthors = function (authors) {
        var nationalities = [];
        authors.forEach(function (author) {
            var normalizedNationality = author.nationality ? author.nationality.toLowerCase() : "";
            if (nationalities.indexOf(normalizedNationality) == -1) {
                nationalities.push(normalizedNationality);
            }
        });
        return nationalities;
    }

    var loadFromJsonFile = function (file) {
        return $http.get(file).then(function (response) {
            return response.data;
        });
    }

});