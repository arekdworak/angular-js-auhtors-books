var vo = {};

vo.Author = function (id, name, nationality, booksCount) {

    this.id = id;

    this.name = name;

    this.nationality = nationality;

    this.booksCount = booksCount;
}

vo.Book = function (id, title, authorId, authorName, year) {

    this.id = id;

    this.title = title;

    this.authorId = authorId;

    this.authorName = authorName;

    this.year = "" + year;
}